/**
 * @author Vanch
 */
(function() {
	'use strict';
	window.Table1 = React.createClass({
	getInitialState:function(){
		var data = [];
	    var tds = [{col:"id"}, {col:"name"}, {col:"src", format:this.formatSrc}, {col:"date"}, {format:this.formatOperations, has_td:true}];
	    var ths = [{colName:"ID"}, {colName:"名字"}, {colName:"来源"}, 
	    	{colName:"注册日期"}, {colName:""}];
	    var ops = [{opid:1,name:"详情",callback:this.handleClick}];
		return {
			start:0,
			data:data,
			ths:ths,
			tds:tds,			
			ops:ops};
	},
	componentWillMount:function(){
		//TODO AJAX, get data before mount
		//...
		
		//test!!!
//		var ajaxdata = [		    		    
//		    {id:1, name:'丁一', date:'2016-08-11', src:'1'},
//				{id:2, name:'不二', date:'2016-08-10', src:'2'},
//		    		{id:3, name:'张三', date:'2016-08-09', src:'1'}
//		    		];
//		this.setState({data:ajaxdata});
	},
	componentDidMount:function(){
		//TODO AJAX, get data after mount
		//...
		
		//test!!!
		var totalNum = 100;
		var numPerPage = 10;
		var ajaxdata = [		    
		    		{id:1, name:'丁一', date:'2016-08-11', src:'2'},
					{id:2, name:'不二', date:'2016-08-10', src:'2'},
		    		{id:3, name:'张三', date:'2016-08-09', src:'1'},
		    		{id:4, name:'李四', date:'2016-08-12', src:'1'},
					{id:5, name:'王五', date:'2016-08-13', src:'2'},
		    		{id:6, name:'小六', date:'2016-08-14', src:'2'},
		    		{id:7, name:'小七', date:'2016-08-14', src:'2'},
		    		{id:8, name:'朱八', date:'2016-08-14', src:'2'},
		    		{id:9, name:'龙九', date:'2016-08-14', src:'1'},
		    		{id:10, name:'杜十', date:'2016-08-14', src:'2'}
		    		];
		this.setState({totalNum:totalNum, numPerPage:numPerPage, data:ajaxdata});    		
	},
	ajaxGetData:function(start, num){
		//TODO AJAX, get data
		//..		
		
		//test data!!!
		var ajaxdata = [
		    		{id:start+1, name:'丁一'+(start+1), date:'2016-08-12', src:'1'},
					{id:start+2, name:'不二'+(start+2), date:'2016-08-13', src:'2'},
		    		{id:start+3, name:'张三' +(start+3), date:'2016-08-14', src:'2'},
		    		{id:start+4, name:'李四'+(start+4), date:'2016-08-12', src:'1'},
					{id:start+5, name:'王五'+(start+5), date:'2016-08-13', src:'2'},
		    		{id:start+6, name:'小六' +(start+6), date:'2016-08-13', src:'2'},
		    		{id:start+7, name:'小七' +(start+7), date:'2016-08-14', src:'2'},
		    		{id:start+8, name:'朱八' +(start+8), date:'2016-08-12', src:'2'},
		    		{id:start+9, name:'龙九'  +(start+9), date:'2016-08-13', src:'1'},
		    		{id:start+10, name:'杜十'  +(start+10), date:'2016-08-16', src:'2'}
		    		];
		    		
		//refresh with the new data.
		this.refreshData(start, ajaxdata);
	},
	refreshData:function(start, data){
		this.setState({start:start, data:data});
	},
	formatSrc:function(data, index){
		var srcs = ["未知","自注册","推荐注册"];
		return srcs[data.src];
	},
	formatOperations:function(data, index){
		var ops = this.state.ops;
		return (<CommonTDOperations key="-2" ops={ops} data={index}/>);
	},
	viewOp:function(index){
		//TODO the view operation.
		alert("viewOp:" +JSON.stringify(this.state.data[index]));
	},
	handleClick:function(opid, index){
		var row_id = index;
		if(opid == 1){
			this.viewOp(index);
		}
	},
	pageAction:function(params){
		this.ajaxGetData(params.start, params.numPerPage);
	},
  render: function() {
    return (
		<div>
			<br></br>
			<div className="dataTable_wrapper">
				<FormatTable ths={this.state.ths} data={this.state.data} tds={this.state.tds}/>
				
				<div>
					<Pages  numPerPage={this.state.numPerPage} totalNum={this.state.totalNum} start={this.state.start} pageAction={this.pageAction}/>
				</div>
			</div>
         </div>
    );
  }
});

})();