/*
 * @auther Vanch
 * 表格td操作项组合组件
 */
 (function() {
	'use strict';
	window.CommonTDOperations = React.createClass({
			render: function() {
				var data = this.props.data;
				var list = this.props.ops.map(function (key, index) {
			      return (			        		        
			          <CommonOperation op={key} key={index} data={data}/>
			      );
			    });
			    
			    return(	
			          	<td className="edit-td">{list}</td>
			    );
			}
	});
	//TODO: 组件自测代码test!!!
})();