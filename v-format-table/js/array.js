/**
 * 
 * @author Vanch
 * Array删除操作工具方法(为列表行删除操作提供支持)
 */
(function () {
Array.prototype.baoremove = function(dx) 
{ 
    if(isNaN(dx)||dx>this.length){return false;} 
    this.splice(dx,1); 
}
Array.prototype.baoremoveall = function() 
{ 
    if(this.length == 0){return false;} 
    this.splice(0,this.length);
}
}());