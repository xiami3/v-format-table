/*
 * @auther Vanch
 * 格式化表格组件
 */
 (function() {
	'use strict';
	window.FormatTable = React.createClass({
	getInitialState:function(){
		return {data:this.props.data,
			ths:this.props.ths,
			tds:this.props.tds};
	},
	componentWillReceiveProps:function(nextProps){
		if(nextProps.data){
			this.setState({data:nextProps.data});
		}
	},
  render: function() {    
    return (
      <table className="table table-striped table-bordered table-hover table-condensed" id="dataTables-example">                        
        <CommonTHead ths={this.state.ths}/>
        <CommonTBody data={this.state.data} tds={this.state.tds}/>
      </table>
    );
  }
});
})();