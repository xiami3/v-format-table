/*
 * @auther Vanch
 * 表格表体tbody组件
 */
(function() {
	'use strict';
	window.CommonTBody = React.createClass({
			render: function() {
				var tds = this.props.tds;
				var list = this.props.data.map(function (key, index) {
			      return (
			        <CommonTR key={index} dataIndex={index} tds={tds} data={key}/>
			      );
			    });
			    return(
			      <tbody>{list}</tbody>
			    )
			}
		});

	//TODO: 组件自测代码test!!!
//	var data = [
//  {_id:1,name:'总库存', user:'无', createDateTime:'北苑店', updateDateTime:'2016-05-18 17:14:02'},
//		{_id:2,name:'超市', user:'总库存', createDateTime:'北苑店', updateDateTime:'2016-05-27 17:14:02'},
//  		{_id:3,name:'库房', user:'总库存', createDateTime:'北苑店', updateDateTime:'2016-05-19 20:14:02'}
//  		];
//  var tds = ["_id", "name", "user", "createDateTime", "updateDateTime"];
//	ReactDOM.render(		 
//  <CommonTBody data={data} tds={tds}/>,
//  document.getElementById('mycontainer')
//);
})();