/*
 * @auther Vanch
 * 选项式表格/表格式选项组件
 */
 (function() {
	'use strict';
	window.OptionTable = React.createClass({
	getDefaultProps:function(){
		return {
			isRadio:false
		};
	},
	getInitialState:function(){
		var options = this.props.options?this.props.options:[];
		var data = [];
		var tds = [];
		var self = this;
		options.map(function(items, i){
			data[i] = {};
			items.map(function(item,j){
				data[i][j] = item;
				tds[j] = {};
				tds[j]['col'] = j;
				tds[j]['format'] = self.formatOption;
			});			
		});
		return {
			isRadio:this.props.isRadio,
			data:data,
			tds:tds
		};
	},
	componentWillReceiveProps:function(nextProps){
		if(nextProps.isRadio){
			this.state.isRadio = nextProps.isRadio;
		}
		if(nextProps.options){
			var options = nextProps.options?nextProps.options:[];
			var data = [];
			var tds = [];
			var self = this;
			options.map(function(items, i){
				data[i] = {};
				items.map(function(item,j){
	//				alert(item +", " +i +", " +j);//test!!!
					data[i][j] = item;
					tds[j] = {};
					tds[j]['col'] = j;
					tds[j]['format'] = self.formatOption;
				});			
			});
//			alert(JSON.stringify(data));
			this.setState({data:data, tds:tds});
		}
		
	},
	onChange:function(e){
//		alert(e.target.checked);
		if(this.props.callback){
			this.props.callback(e.target.value, e.target.checked);
		}
	},
	formatOption:function(data,checkIndex, col){
		if(!data[col]){
			return "";
		}
		if(this.state.isRadio == true){
			return (<span ><input type="radio" name="myRadio" defaultValue={data[col]['value']} defaultChecked={data[col]['checked']} onChange={this.onChange}></input>{data[col]['label']}</span>);
		}else
		{
			return (<span ><input type="checkbox" name="myCheck" defaultValue={data[col]['value']} defaultChecked={data[col]['checked']} onChange={this.onChange}></input>{data[col]['label']}</span>);
		}
	},
  render: function() {    
    return (
      <table className="table-condensed" style={{marginTop:"-5px"}}>
        <CommonTBody data={this.state.data} tds={this.state.tds}/>
      </table>
    );
  }
});
})();