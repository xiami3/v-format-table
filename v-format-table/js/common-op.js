/*
 * @auther Vanch
 * 表格td操作项单元组件
 */
(function() {
	'use strict';
	window.CommonOperation = React.createClass({
			handleClick: function(){
				var data = this.props.data;
				var op = this.props.op;
				op.callback(op.opid, data);
			},
			render: function() {
				var data = this.props.data;			    
				var op = this.props.op;
			    return(
			      <a onClick={this.handleClick} opid={op.opid}><i className="fa fa-fw" title="edit"/>{op.name}</a>
			    )
			}
		});

	//TODO: 组件自测代码test!!!
})();